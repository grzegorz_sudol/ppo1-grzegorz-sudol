#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <ctime>

using namespace std;

#define STUDENTS_COUNT 10

class Student {
	public:
		string studentNo;
		string studentName;
		string studentSurname;
		string studentAc;
		
			
		void setStudentNo(string studentNo) {
			this->studentNo = studentNo; 
		}
		string getStudentNo() {
			return this->studentNo;
		}

		void setStudentName(string studentName) {
		this->studentName = studentName;
		}
		
		string getStudentName() {
			return this->studentName;
		}
		
		void setStudentSurname(string studentSurname) {
		this->studentSurname = studentSurname;
		}
		
		string getStudentSurname() {
			return this->studentSurname;
		}
	
		void setStudentAc(string studentAc) {
		this->studentAc = studentAc;
		}
		
		string getStudentAc() {
			return this->studentAc;
		}
};

//------------ numer ------------//
string getRandomStudentNumber() {
	ostringstream ss;
	int randomNumber = rand() % 2000 + 37000;
	
	ss << randomNumber;
	
	return ss.str();
}
//------------ imie ------------//
string getRandomStudentName() {
	ostringstream name;
    string tabName[] = {"Adam", "Marek", "Damian", "Anna", "Henryk", "Aleks", "Daria", "Maria", "Ola", "Norbert"};
    name << tabName[rand() % 10];
	
	return name.str();
}
//------------ nazwisko ------------//
string getRandomStudentSurname() {
	ostringstream sur;
    string tabSurname[] = {"Nowak", "Kowal", "Lis", "Adamowicz", "Czeczko", "Lipiak", "Pluta", "Skrobot", "Tomiak", "Bogusz"};
    sur << tabSurname[rand() % 10];
	
	return sur.str();
}
//------------ aktywny/nieaktywny ------------//
string getRandomStudentAc() {
	ostringstream ac;
    string tabAc[] = {"Aktywny", "Nieaktywny"};
    ac << tabAc[rand() % 2];
	
	return ac.str();
}

int main() {
	vector<Student> students;
	
	for(int i = 0; i < STUDENTS_COUNT; i++) {
		Student student;
		student.setStudentNo(getRandomStudentNumber());
		student.setStudentName(getRandomStudentName());
		student.setStudentSurname(getRandomStudentSurname());
		student.setStudentAc(getRandomStudentAc());
		students.push_back(student);
	}

	
	cout  << "Students group have been filled." << endl << endl;
	
	for(int i = 0; i < students.size(); i++) {
		Student student = students.at(i);
		if(student.studentAc == "Aktywny")
		cout << student.getStudentSurname() <<" "<< student.getStudentName() << " (" << student.getStudentNo() <<") "<<  endl;
 }


	

	return 0;
}

